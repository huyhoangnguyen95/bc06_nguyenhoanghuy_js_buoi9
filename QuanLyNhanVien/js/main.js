//

let dsnv = [];
const LOCAL_JSON = "LOCAL_JSON";
const VALIDATE_STRING = "không được để trống";

// lấy dữ liệu từ localStorage
let JSONdata = localStorage.getItem(LOCAL_JSON);
// convert từ chuỗi JSON lại thành array
// do save xuống localStorage không có method nên phải duyệt lại mảng để tạo lại object từ lớp đối tượng NhanVien
dsnv = JSON.parse(JSONdata).map(function (item) {
  return new NhanVien(
    item.taiKhoan,
    item.hoTen,
    item.email,
    item.pass,
    item.ngayLam,
    item.luongCoBan,
    item.chucVu,
    item.gioLam
  );
});
renderDSNV(dsnv);

function themNhanVien() {
  // lấy thông tin từ form
  let nhanVien = layThongTinTuForm();

  // validate tài khoản
  let isValid =
    kiemTraRong(nhanVien.taiKhoan, "tbTKNV", `Tài khoản ${VALIDATE_STRING}`) &&
    kiemTraDoDai(nhanVien.taiKhoan, "tbTKNV", 4, 6);

  // validate họ tên
  isValid &=
    kiemTraRong(nhanVien.hoTen, "tbTen", `Họ tên ${VALIDATE_STRING} `) &&
    kiemTraUsername(nhanVien.hoTen, "tbTen");

  // validate email
  isValid &=
    kiemTraRong(nhanVien.email, "tbEmail", `Email ${VALIDATE_STRING} `) &&
    kiemTraEmail(nhanVien.email);

  // validate password
  isValid &=
    kiemTraRong(nhanVien.pass, "tbMatKhau", ` Mật khẩu ${VALIDATE_STRING} `) &&
    kiemTraPassword(nhanVien.pass);

  // validate ngày làm
  isValid &=
    kiemTraRong(nhanVien.ngayLam, "tbNgay", ` Ngày làm ${VALIDATE_STRING} `) &&
    kiemTraNgay(nhanVien.ngayLam);

  // validate lương cơ bản
  isValid &=
    kiemTraRong(nhanVien.luongCoBan, "tbLuongCB", `Lương ${VALIDATE_STRING}`) &&
    kiemTraLuong(nhanVien.luongCoBan, "tbLuongCB", 1000000, 20000000);

  // validate chức vụ
  isValid &= kiemTraChucVu(nhanVien.chucVu);

  // validate giờ làm
  isValid &=
    kiemTraRong(nhanVien.gioLam, "tbGiolam", `Giờ làm ${VALIDATE_STRING}`) &&
    kiemTraGioLam(nhanVien.gioLam, 80, 200);

  if (isValid) {
    //   thêm nhân viên
    dsnv.push(nhanVien);
    console.log("dsnv: ", dsnv);

    // convert array thành chuỗi JSON
    let dataJSON = JSON.stringify(dsnv);
    localStorage.setItem(LOCAL_JSON, dataJSON);

    //   renderDSNV
    renderDSNV(dsnv);
    resetForm();
  }
}

function xoaNhanVien(id) {
  let viTri = -1;

  for (let i = 0; i < dsnv.length; i++) {
    let item = dsnv[i];
    if (item.taiKhoan == id) {
      viTri = i;
    }
  }

  if (viTri != -1) {
    dsnv.splice(viTri, 1);
    // convert array thành chuỗi JSON
    let dataJSON = JSON.stringify(dsnv);
    localStorage.setItem(LOCAL_JSON, dataJSON);
    renderDSNV(dsnv);
  }
}

function suaNhanVien(id) {
  console.log("id: ", id);
  let viTri = dsnv.findIndex(function (item) {
    return item.taiKhoan == id;
  });

  let nhanVien = dsnv[viTri];
  showThongTinLenForm(nhanVien);
  // document.getElementById("myModal").style.display = "block";
  // document.getElementById("myModal").style.opacity = "1";
  // document.querySelector(".modal-dialog").style.transform = "translate(0,0)";
}

function capNhatNhanVien() {
  let nv = layThongTinTuForm();
  let viTri = dsnv.findIndex(function (item) {
    return item.taiKhoan == nv.taiKhoan;
  });
  dsnv[viTri] = nv;
  // convert array thành chuỗi JSON
  let dataJSON = JSON.stringify(dsnv);
  localStorage.setItem(LOCAL_JSON, dataJSON);
  renderDSNV(dsnv);
}

function resetForm() {
  document.getElementById("form").reset();
}

// function dongForm() {
//   document.getElementById("myModal").style.display = "none";
//   document.getElementById("myModal").style.opacity = "0";
// }

function filterNhanVien() {
  var value = document.getElementById("searchName").value;
  var newArray = [];
  if (value) {
    console.log("value: ", value);
    for (var i = 0; i < dsnv.length; i++) {
      var item = dsnv[i];
      console.log("item: ", item);
      var stringXepLoai = item.xepLoai();
      console.log("stringXepLoai: ", stringXepLoai);
      if (stringXepLoai.includes(value)) {
        var nv = new NhanVien(
          item.taiKhoan,
          item.hoTen,
          item.email,
          item.pass,
          item.ngayLam,
          item.luongCoBan,
          item.chucVu,
          item.gioLam
        );
        newArray.push(nv);
      }
    }
    console.log("newArray: ", newArray);
  } else {
    console.log("0 giá trị");
  }
}

document.getElementById("btnTimNV").addEventListener("click", filterNhanVien);
