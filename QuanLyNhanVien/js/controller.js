//

function layThongTinTuForm() {
  let taiKhoan = document.getElementById("tknv").value;
  let hoTen = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let pass = document.getElementById("password").value;
  let ngayLam = document.getElementById("datepicker").value;
  let luongCoBan = document.getElementById("luongCB").value;
  let chucVu = document.getElementById("chucvu").value;
  let gioLam = document.getElementById("gioLam").value;

  //   return {
  //     taiKhoan: taiKhoan,
  //     hoTen: hoTen,
  //     email: email,
  //     ngayLam: ngayLam,
  //     luongCoBan: luongCoBan,
  //     chucVu: chucVu,
  //     gioLam: gioLam,
  //   };

  return new NhanVien(
    taiKhoan,
    hoTen,
    email,
    pass,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
}

function renderDSNV(dsnv) {
  let contentHTML = "";
  dsnv.forEach(function (item) {
    contentHTML += ` 
        <tr>
            <td>${item.taiKhoan}</td>
            <td>${item.hoTen}</td>
            <td>${item.email}</td>
            <td>${item.ngayLam}</td>
            <td>${item.chucVu}</td>
            <td>${item.tinhTongLuong()}</td>
            <td>${item.xepLoai()}</td>
            <td> 
              <button onclick="suaNhanVien('${
                item.taiKhoan
              }')" class="btn btn-warning" data-target="#myModal">Sửa</button>
              <button onclick="xoaNhanVien('${
                item.taiKhoan
              }')" class="btn btn-danger">Xóa</button>

            </td>
            
        </tr>
        `;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nhanVien) {
  document.getElementById("tknv").value = nhanVien.taiKhoan;
  document.getElementById("name").value = nhanVien.hoTen;
  document.getElementById("email").value = nhanVien.email;
  document.getElementById("password").value = nhanVien.pass;
  document.getElementById("datepicker").value = nhanVien.ngayLam;
  document.getElementById("luongCB").value = nhanVien.luongCoBan;
  document.getElementById("chucvu").value = nhanVien.chucVu;
  document.getElementById("gioLam").value = nhanVien.gioLam;
}
