//

function NhanVien(
  taiKhoan,
  hoTen,
  email,
  pass,
  ngayLam,
  luongCoBan,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.hoTen = hoTen;
  this.email = email;
  this.pass = pass;
  this.ngayLam = ngayLam;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tinhTongLuong = function () {
    switch (this.chucVu) {
      case "Sếp":
        return this.luongCoBan * 3;
      case "Trưởng phòng":
        return this.luongCoBan * 2;
      default:
        return this.luongCoBan * 1;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Xuất sắc";
    } else if (this.gioLam >= 176) {
      return "Giỏi";
    } else if (this.gioLam >= 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}
