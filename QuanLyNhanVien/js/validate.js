//

function showMessage(idErr, message) {
  document.getElementById(idErr).innerHTML = message;
}

function kiemTraRong(value, idErr, message) {
  if (value.length != 0) {
    showMessage(idErr, "");
    return true;
  } else {
    document.getElementById(idErr).style.display = "block";
    showMessage(idErr, message);
    return false;
  }
}

function kiemTraDoDai(value, idErr, min, max) {
  if (value.length >= min && value.length <= max) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, `Tối đa từ ${min} - ${max} kí tự`);
    return false;
  }
}

function kiemTraUsername(value, idErr) {
  var name =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/g;
  if (name.test(value)) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, "Tên nhân viên phải là chữ");
    return false;
  }
}

function kiemTraEmail(value) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(value)) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email không đúng định dạng");
    return false;
  }
}

function kiemTraPassword(value) {
  var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{6,10}$/;
  if (re.test(value)) {
    showMessage("tbMatKhau", "");
    return true;
  } else {
    showMessage(
      "tbMatKhau",
      "Mật khẩu từ 6-10 kí tự (chứa 1 kí tự số , 1 kí tự in hoa , 1 kí tự đặc biệt) "
    );
    return false;
  }
}

function kiemTraNgay(value) {
  var date = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;

  if (date.test(value)) {
    showMessage("tbNgay", "");
    return true;
  } else {
    showMessage("tbNgay", "Ngày phải đúng định dạng mm/dd/yyyy");
    return false;
  }
}

function kiemTraLuong(value, idErr, min, max) {
  if (value >= min && value <= max) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, `Lương từ ${min} - ${max}`);
    return false;
  }
}

function kiemTraChucVu(value) {
  if (value != "Chọn chức vụ") {
    showMessage("tbChucVu", "");
    return true;
  } else {
    document.getElementById("tbChucVu").style.display = "block";
    showMessage("tbChucVu", "Bạn chưa chọn chức vụ");
    return false;
  }
}

function kiemTraGioLam(value, min, max) {
  if (value >= min && value <= max) {
    showMessage("tbGiolam", "");
    return true;
  } else {
    showMessage("tbGiolam", `Số giờ làm từ ${min} - ${max}`);
    return false;
  }
}
